# edium

Edium is a package of two useful command line tools for editing files in your
favorite text editor.  The first one, ed, allows you to edit a file with your
$EDITOR; however, if no file is given then it opens the whole directory into
a tree view.  The other program, ED, simply toggles your $EDITOR.

## Installation

To install edium add this line to your shells rc files(`.bashrc` or `.zshrc`):

	source ~/path/to/edium.sh

## Contributing

I am open to pull requests just as long as you know [how to to write a commit
message](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html).
Please report any bugs/issues you find. I am also very open to new ideas and
critique, see Contact section below on how to contact me.

## Contact

If you have any questions or comments please contact me at edvb54@gmail.com or
leave a comment wherever.

-ED

## Licence

GPL v3 License

